var DOB = "February 19, 1998"
var msBetweenDOBand1970 = Date.parse(DOB)
var msBetweenNowand1970 = Date.now()
var ageInMs = msBetweenNowand1970-msBetweenDOBand1970

var ms = ageInMs
var second = 1000
var minute = second*60
var hour = minute*60
var day = hour*24
var month = day*30
var year = day*365

var years = Math.round(ms/year)

let age = years

$( document ).ready(() => {
    $("#ageId").text(age)
})

$(function() {
    $("#progress-bar-green").addClass("progress-bar-green");
});

$(window).scroll(function() {

    // selectors
    var $window = $(window),
        $body = $('body'),
        $panel = $('.panelBackgroundColors');

    // Change 33% earlier than scroll position so colour is there when you arrive.
    var scroll = $window.scrollTop() + ($window.height() / 3);

    $panel.each(function () {
        var $this = $(this);

        // if position is within range of this panel.
        // So position of (position of top of div <= scroll position) && (position of bottom of div > scroll position).
        // Remember we set the scroll to 33% earlier in scroll var.
        if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

            // Remove all classes on body with color-
            $body.removeClass(function (index, css) {
                return (css.match (/(^|\s)color-\S+/g) || []).join(' ');
            });

            // Add class of currently active div
            $body.addClass('color-' + $(this).data('color'));
        }
    });

}).scroll();